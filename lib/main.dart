import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:holograms/src/pages/home_page.dart';
import 'package:holograms/src/pages/login_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Holograms',
      initialRoute: 'login',
      routes: {
        'home'           : ( BuildContext context ) => HomePage(),
        'login'          : ( BuildContext context ) => LoginPage(),
      },
      theme: ThemeData(
          primaryColor: Color(0xff21254A),
        ),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (context,AsyncSnapshot<FirebaseUser> snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting)
          return Container(
            child: Text('Loading'),
          );
        if(!snapshot.hasData || snapshot.data == null)
          return LoginPage();
       else{
          return HomePage();
       }
      },
    );
  }
}