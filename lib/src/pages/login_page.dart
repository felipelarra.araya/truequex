import 'package:flutter/material.dart';
import 'package:holograms/src/pages/home_page.dart';
import 'package:holograms/src/utils/firebase_auth.dart';
 
void main() => runApp(LoginPage());
 
class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return Scaffold(
      backgroundColor: Color(0xff21254A),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            _logo(),
            SizedBox(height: 48.0),
            
            SizedBox(height: 8.0),
           
            SizedBox(height: 24.0),
            _loginButtonGoogle(context),
            _loginButtonFacebook(),
           
          ],
        ),
      ),
    );
  }

Widget _logo(){
  return Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: FlutterLogo(
          size: 100.0,
        )
      ),
    );
}

Widget _loginButtonGoogle(BuildContext context){
  return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
      onPressed: () {
        signInWithGoogle().whenComplete(() {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return HomePage();
              },
            ),
          );
        });
      },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Ingresar con Gmail', style: TextStyle(color: Colors.orangeAccent)),
        
      ),
    );
}

Widget _loginButtonFacebook(){
  return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: (null),
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('ingresar con Facebook', style: TextStyle(color: Colors.blueAccent)),
      ),
    );
}



}
